# Gaze Data Normalization

Sample python code for gaze data normalization, with dlib-based facial landmark detection

The code is based on [this implementation](https://www.mpi-inf.mpg.de/departments/computer-vision-and-machine-learning/research/gaze-based-human-computer-interaction/revisiting-data-normalization-for-appearance-based-gaze-estimation/), and licensed under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).

## Notes

- You first need to download dlib models (run data/download_models.sh)
- Usage: `python normalize_data.py -i <input directory> -o <output directory>`
- To be accurate, you also need to feed your camera calibration data. Please refer to available command-line options
- This code currently just normalizes and saves the output (normalized) images, and the function to normalize gaze metadata is not yet implemented